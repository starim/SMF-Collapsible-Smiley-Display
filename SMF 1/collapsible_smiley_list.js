/**
 * @author: Slixbits, © 2011
 * Licensed under LGPLv3 or later
 * 
 * Part of the Collapsible Smiley List plugin for SMF.
 * This file contains the Javascript that initializes and manages the smiley display on the post or private message page.
 * 
 */


// implement the indexOf() function for arrays in browsers that don't have it (in particular, Internet Explorer)
if(!Array.indexOf){
	Array.prototype.indexOf = function(element){
		for(var index = 0; index < this.length; index++){
			if(this[index] === element){
				return index;
			}
		}
		
		return -1;
	}
}


$(document).ready(function () {

	"use strict";


	/*
	 * Setup Section - defines functions, objects, and constants used throughout this script
	 */
	
	// if true, debug messages will be printed into the post composition textarea, otherwise debug messages will be stored in the window.smiley_debugMessages array
	var DEBUG_MODE = true;

	// the duration, in milliseconds, of the animation to show and hide smiley groups
	var HIDE_GROUP_SPEED = 400;

	// for better interactivity, open smiley groups wait HIDE_GROUP_DELAY milliseconds after the user has moused out of the smiley group before
	// hiding the smiley group
	// smileyGroupsCountingDownToHide is a key-value store mapping a group name to the active countdown timer to hiding that group
	var HIDE_GROUP_DELAY = 100;
	var smileyGroupsCountingDownToHide = {};
	
	// calculate the URL of this plugin's resources folder from the theme directory's URL, which is passed by PHP via setting a global Javascript variable
	var RESOURCE_URL = window.smiley_environmentProperties["theme URL"] + "/collapsible_smiley_list/resources/";
	
	// used to reference the text input element in which the smileys should insert their codes when clicked
	var POST_FORM = window.smiley_environmentProperties["post form"];
	var POST_BOX_NAME = window.smiley_environmentProperties["post box name"];
	

	// prints debug messages to the textarea where posts are composed if DEBUG_MODE is true, otherwise saves debug messages in the window.smiley_debugMessages array
	var debug = function(debugMessage) {
		if(DEBUG_MODE	)
			$(".editor").append(debugMessage + "\n");
		else {
			if(typeof(window.smiley_debugMessages) == "undefined")
				window.smiley_debugMessages = new Array();
			
			window.smiley_debugMessages.push(debugMessage);
			
			// keep the debug messages array to last twenty messages to avoid consuming too much memory
			if(window.smiley_debugMessages.length > 20)
				window.smiley_debugMessages.shift();
		}
	}
	
	
	// initialize the object that tracks the user's settings and load any saved settings this user has
	var userPreferences = new UserPreferences();

	
	// initializes the newly created smiley container to be a favorite
	// this function should be called when a smiley container is first created and is one of the user's default favorites
	var initializeFavorite = function(smileyContainer) {
		smileyContainer.addClass("smiley_loaded");
		
		// hide the help text in the Favorites area
		$("#smiley_list #smiley_favorites #smiley_favorites_help_text").css("display", "none");		
	}
	
	// adds the passed smiley containers to the Favorites list
	// the smiley containers must be passed in a jQuery object
	// triggeredProgrammatically is a boolean indicating whether this event was triggered programmatically; if so the user's settings must be updated
	// here since the jQuery UI elements won't have fired events showing their updated lists
	var addToFavorites = function(smileyContainers, triggeredProgrammatically) {

		$(smileyContainers).each(function() {
			$(this).addClass("smiley_loaded");
			if(triggeredProgrammatically)
				$(this).detach().appendTo("#smiley_list #smiley_favorites");
		});
		
		if(triggeredProgrammatically) {
			$("#smiley_list .smiley_section_content").each(function() {
				$(this).triggerHandler("programmaticUpdate");
			});
		}

		// hide the help text in the Favorites area
		$("#smiley_list #smiley_favorites #smiley_favorites_help_text").css("display", "none");
	};
	
	
	// removes the passed smileys from the Favorites list
	// the smileys must be passed in a jQuery object
	// triggeredProgrammatically is a boolean indicating whether this event was triggered programmatically; if so the user's settings must be updated
	// here since the jQuery UI elements won't have fired events showing their updated lists
	var removeFromFavorites = function(smileyContainers, triggeredProgrammatically) {

		$(smileyContainers).each(function() {
			userPreferences.removeFavorite($(this).attr("id"));
			if(triggeredProgrammatically)
				$(this).detach().appendTo("#smiley_list #smiley_unloaded");
			setSmileyGroupVisibility($(this).attr("id"), false);
		});
		
		if(triggeredProgrammatically) {
			$("#smiley_list .smiley_section_content").each(function() {
				$(this).triggerHandler("programmaticUpdate");
			});
		}

		// show the help text in the target area if there are no more smileys
		if($("#smiley_list #smiley_favorites .smiley_set").length == 0)
			$("#smiley_list #smiley_favorites #smiley_favorites_help_text").css("display", "block");
	};
	
	
	// sets whether to show or hide the drop down with the extra smileys for each group
	// groupName is the name of the smiley group, which is also the id of the element that contains the group
	// visibility is a boolean indicating whether the drop down should be shown or hidden
	// immediate is an optional boolean that if true causes the visibility change to happen immediately without an animation or waiting on any timers (defaults to false)
	var setSmileyGroupVisibility = function(groupName, visibility, immediate) {
		
		if(immediate == null)
			immediate = false;
	
		// cancel any pending hide effects on this group
		if(typeof(smileyGroupsCountingDownToHide[groupName]) != "undefined") {
			clearTimeout(smileyGroupsCountingDownToHide[groupName]);
			delete smileyGroupsCountingDownToHide[groupName];
		}
		
		var movingParts = $("#" + groupName).find(".smiley_dropDown");
		if(visibility) {
			movingParts.stop(true, true);
			movingParts.fadeIn(immediate ? 0 : HIDE_GROUP_SPEED);
		}
		else {
			// waits a short time before actually hiding the drop down, to avoid user frustration when the drop down hides if the mouse barely leaves the drop down's area
			if(immediate)
				movingParts.fadeOut(0);
			else {
				setTimeout(function() {
					delete smileyGroupsCountingDownToHide[groupName];
					movingParts.stop(true, true);
					movingParts.fadeOut(HIDE_GROUP_SPEED);
				},  HIDE_GROUP_DELAY);
			}
		}
	};
	
	
	
	
	/*
	 * Implementation Section - the actual creation and management of the collapsible smiley list is done here
	 */
	
	
	// give the smiley area an id so that it can be easily found in the DOM
	$("#postmodify .editor").parent().parent().prev().children("td:last").attr("id", "smiley_list");
	
	
	// create the Favorites section for smileys the user has marked as frequently used and should be loaded immediately
	
	$("<div>").addClass("smiley_section").appendTo("#smiley_list");
	$("<div>").addClass("smiley_section_heading").html("<h5>Favorites</h5>").appendTo("#smiley_list > .smiley_section:last-child");
	$("<div>").attr("id", "smiley_favorites").addClass("smiley_section_content").appendTo("#smiley_list > .smiley_section:last-child");
	$("<p>").attr("id", "smiley_favorites_help_text").text("Drag smileys from the unloaded section to here to mark them as your favorites. Favorites will always be loaded immediately when composing a post. Smileys not marked as favorites can still be loaded by mousing over them.").appendTo("#smiley_favorites");
	
	// make the Favorites section a drop target for smileys dragged from the Unloaded section
	$("#smiley_list #smiley_favorites").sortable({
		"connectWith": "#smiley_list #smiley_unloaded",
		"receive": function(event, ui) {
			addToFavorites(ui.item, false);
		},
		"remove": function(event, ui) {
			removeFromFavorites(ui.item, false);
		},
		"revert": 50,
		"delay": 100,
		"items": ".smiley_set",
		"helper": "clone",
		"update": function(event, ui) {
			setSmileyGroupVisibility(ui.item.attr("id"), false, true);
			userPreferences.setFavorites($(this).sortable('toArray'));
		},
		"start": function(event, ui) {
			// hide the drop-down when dragging a smiley group
			ui.helper.find(".smiley_dropDown").css("display", "none");
			// hide the mouse cursor while dragging
			ui.helper.find("*").css("cursor", "none");
		}
	});
	$("#smiley_list #smiley_favorites").bind("programmaticUpdate", function() {
		userPreferences.setFavorites($(this).sortable('toArray'));
	});
	
	
	// set up the toggle control for visibility of the controls section
	
	$('<div id="smiley_controls_toggle_container"><span id="smiley_controls_toggle"><span class="smiley_toggle_text_expand">Expand Smiley Controls</span><span class="smiley_toggle_text_collapse">Collapse Smiley Controls</span></span></div>').appendTo("#smiley_list");

	// determine whether to show or hide the controls section by default; if the Favorites list is empty then the controls will be shown regardless of the default setting as showing an empty box with
	// no controls to add smileys to it is probably confusing for the user
	var defaultControlSectionVisibility = userPreferences.getControlsVisibility() || userPreferences.isFavoritesEmpty();
	$("#smiley_list #smiley_controls_toggle").addClass(defaultControlSectionVisibility ? "smiley_toggle_state_expand" : "smiley_toggle_state_collapse").data("expand_state", defaultControlSectionVisibility);
	$("#smiley_list #smiley_controls_toggle").bind("click", function() {
	
		var goToCollapseState = $(this).data("expand_state");
		userPreferences.setControlsVisibility(!goToCollapseState);
		
		if(goToCollapseState)
			$(this).removeClass("smiley_toggle_state_expand").addClass("smiley_toggle_state_collapse");
		else
			$(this).removeClass("smiley_toggle_state_collapse").addClass("smiley_toggle_state_expand");
		
		$("#smiley_list #smiley_controls").slideToggle("100");
		$(this).data("expand_state", !goToCollapseState);
	});
	
	
	// create a controls section
	
	$("<div>").attr("id", "smiley_controls").appendTo("#smiley_list");
	$('<span id="smiley_add_all_to_favorites" class="smiley_add_or_remove_all_controls">Add All</span>').appendTo("#smiley_list #smiley_controls");
	$('<span id="smiley_remove_all_from_favorites" class="smiley_add_or_remove_all_controls">Remove All</span>').appendTo("#smiley_list #smiley_controls");
	if(!defaultControlSectionVisibility)
		$("#smiley_list #smiley_controls").hide(0);
	
	$("#smiley_list #smiley_add_all_to_favorites").bind("click", function() { addToFavorites($("#smiley_list #smiley_unloaded .smiley_set"), true) });
	$("#smiley_list #smiley_remove_all_from_favorites").bind("click", function() { removeFromFavorites($("#smiley_list #smiley_favorites .smiley_set"), true) });
	
	
	// create the Unloaded section for smileys the user hasn't marked as a favorite and thus will be lazy-loaded
	
	$("<div>").addClass("smiley_section").appendTo("#smiley_controls");
	$("<div>").addClass("smiley_section_heading").html('<h5>Unloaded</h5>').appendTo("#smiley_list #smiley_controls .smiley_section:last-child");
	$("<div>").attr("id", "smiley_unloaded").addClass("smiley_section_content").appendTo("#smiley_list #smiley_controls .smiley_section:last-child");

	// make the Unloaded section a drop target for the smileys dragged from the Favorites section
	$("#smiley_list #smiley_unloaded").sortable({
		"connectWith": "#smiley_list #smiley_favorites",
		"revert": 100,
		"delay": 100,
		"items": ".smiley_set",
		"helper": "clone",
		"update": function(event, ui) {
			setSmileyGroupVisibility(ui.item.attr("id"), false, true);
			userPreferences.setUnloadedOrder($(this).sortable('toArray'));
		},
		"start": function(event, ui) {
			// hide the drop-down when dragging a smiley group
			ui.helper.find(".smiley_dropDown").css("display", "none");
			// hide the mouse cursor while dragging
			ui.helper.find("*").css("cursor", "none");
		}
	});
	$("#smiley_list #smiley_unloaded").bind("programmaticUpdate", function() {
		userPreferences.setUnloadedOrder($(this).sortable('toArray'));
	});
	
	
	
	
	if(typeof(window.collapsible_smiley_list) == "undefined" || window.collapsible_smiley_list == null) {
		debug("No smiley list could be found. The expected smiley container variable is " + (typeof(window.collapsible_smiley_list) == "undefined" ? "undefined" : "null") + ".");
		return;
	}
	
	// create a sorted array of smiley group names that will be used to populate the Favorites and Unloaded sections in the order the user expects
	var sortedGroupNames = new Array();
	for(var key in window.collapsible_smiley_list)
		sortedGroupNames.push(key);
	sortedGroupNames.sort(function(a, b) {
	
		// the smiley group names are saved in the user preferences under their long names, so the short names need to be converted to the long form by adding the "smiley_" prefix
		a = "smiley_" + a;
		b = "smiley_" + b;
	
		var isFavoriteA = userPreferences.isFavorite(a);
		var isFavoriteB = userPreferences.isFavorite(b);
		
		// separate the favorites from the non-favorites
		if((isFavoriteA && !isFavoriteB) || (!isFavoriteA && isFavoriteB)) {
			if(isFavoriteA)
				return -1;
			else
				return 1;
		}
	
		if(isFavoriteA) {
			// both smileys are favorites, sort by their position in the Favorites list
			var favoritesList = userPreferences.getFavorites();
			return (favoritesList.indexOf(a) - favoritesList.indexOf(b));
		}
		else {
			// both smileys are non-favorites, sort by their position in the Unloaded list
			// new smileys that haven't been recorded in the Unloaded list should be sorted into the last position
			
			var unloadedList = userPreferences.getUnloadedOrder();
			
			var indexOfA = unloadedList.indexOf(a);
			if(indexOfA == -1)
				indexOfA = unloadedList.length;
			
			var indexOfB = unloadedList.indexOf(b);
			if(indexOfB == -1)
				indexOfB = unloadedList.length;
			
			return (indexOfA - indexOfB);
		}
	});
	
	// organize SMF's smiley list into the desired structure
	for(var i in sortedGroupNames) {
		var smileyGroupShortName = sortedGroupNames[i];
		for(var smileyName in window.collapsible_smiley_list[smileyGroupShortName]) {

			var smileyGroupName = "smiley_" + smileyGroupShortName;
			var smileyProperties = window.collapsible_smiley_list[smileyGroupShortName][smileyName];
			smileyProperties["code"] = ":" + smileyGroupShortName + "|" + smileyName + ":";
		
			// if there is no group with this smiley's group name, create it
			// otherwise add the smiley to its group
			var thisSmileyGroupContainer = userPreferences.isFavorite(smileyGroupName) ? "#smiley_favorites" : "#smiley_unloaded";
			
			// defines the function attached to smiley images that loads the smiley image
			var loadSmiley = function() {
				var smileyElement = $(this).find("img");
				var smileyURL = $(this).data("smileyProperties")["URL"];
				
				var smileySet = smileyElement.parents(".smiley_set");
				
				var smileyImageLoader = new Image();
				smileyImageLoader.onload = function() {
					smileyElement.attr("src", smileyURL).removeClass("smiley_unloaded");
					
					// expand the width of all the smiley wrappers if this smiley is wider than the others
					if(smileyImageLoader.width > smileySet.data("max_width")) {
						smileySet.data("max_width", smileyImageLoader.width);
						smileySet.find(".smiley_wrapper").css("width", smileyImageLoader.width);
					}

					// Internet Explorer doesn't resize the image element when a new src points to an image with a different size, so it must be manually resized
					smileyElement.css("height", smileyImageLoader.height);
					smileyElement.css("width", smileyImageLoader.width);

					// if all the smileys in this group are now loaded, set a CSS class on the marquee marking this group as fully loaded which will be used to apply a visual effect
					if(smileySet.find(".smiley_unloaded").length == 0)
						smileySet.addClass("smiley_loaded");
				};
				smileyImageLoader.src = smileyURL;			
			};
			
			
			// builds the prototype element used as a base for all smilies
			var smileyPrototype = $("<span>").addClass("smiley_wrapper").attr("title", smileyProperties["description"]).data("smileyProperties", smileyProperties).bind("click", function() {
				// insert the smiley code into the post form
				replaceText(" " + $(this).data("smileyProperties")["code"], document.forms[POST_FORM][POST_BOX_NAME]);
				return false;
			});
			smileyPrototype.one("smiley_lazyLoad", loadSmiley);
			$("<img>").attr("src", RESOURCE_URL + "loading_placeholder.gif").attr("alt", smileyProperties["code"]).addClass("smiley_unloaded").appendTo(smileyPrototype);
			
			
			// if this is the first smiley in its group, initialize the smiley group container and create a smiley marquee; otherwise just add the smiley to the existing group container
			if($("#smiley_list " + thisSmileyGroupContainer + " #" + smileyGroupName).length == 0) {
				var smileyGroup = $("<span>").attr("id", smileyGroupName).addClass("smiley_set").appendTo(thisSmileyGroupContainer);
				smileyGroup.data("max_width", -1);
				var smileySubGroup = $("<span>").addClass("smiley_dropDown").appendTo(smileyGroup);
				
				// construct the image element for this marquee smiley and attach it to the DOM
				smileyPrototype.insertBefore(smileySubGroup);
				smileyPrototype.triggerHandler("smiley_lazyLoad");
				
				if(userPreferences.isFavorite(smileyGroupName))
					initializeFavorite(smileyGroup);
				else {
					// for non-favorites, load the smileys in this group only after the user mouses over the marquee smiley
					smileyPrototype.parents(".smiley_set").one("mouseover", function() {
						$(this).find(".smiley_dropDown .smiley_wrapper").each(function() {
							$(this).triggerHandler("smiley_lazyLoad");
						});
					});
				}
		
				smileyGroup.hover(
					// called when the user mouses into the smiley group
					function() {
						setSmileyGroupVisibility($(this).attr("id"), true);
					},
					// called when the user mouses out of the smiley group
					function() {
 						setSmileyGroupVisibility($(this).attr("id"), false);
					}
				);
			}
			else {
				// attach the smiley to the DOM
				smileyPrototype.appendTo("#smiley_list #" + smileyGroupName + " .smiley_dropDown");
				
				// load the smiley immediately if it's a favorite
				if(userPreferences.isFavorite(smileyGroupName))
					smileyPrototype.triggerHandler("smiley_lazyLoad");
			}
		}
	}
	
	
	// set any smiley groups in the Unloaded section with only a marquee smiley to the loaded state
	$("#smiley_list #smiley_unloaded .smiley_set").each(function() {
		if($(this).find(".smiley_dropDown").children("img").length == 0)
			$(this).addClass("smiley_loaded");
	});
});
