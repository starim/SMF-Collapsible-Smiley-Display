Collapsible Smiley List for Simple Machines Forums (SMF)

Version: 1.5
Date: August 13, 2011
Compatibility: 2.0+

Based on the original Collapsible Smiley List by Wolfey
Updated by Slixbits

Licensed under the Lesser General Public License (LGPL) version 3 or later.
See http://www.gnu.org/licenses/lgpl-3.0.txt or the attached license file for
details.


This plugin for the Simple Machines Forums (SMF) is meant to provide an organized
smiley display that can handle hundreds or more smileys. To use this plugin
effectively, the smileys must be grouped and their codes must have the format:

:group_name|smiley_name:

where all smileys in a group have the same group_name, and each smiley in a group
has a different smiley_name. For example, for a webcomic forum it's common to
create smileys of the comic characters so it would be sensible to group smileys
by character, with the smiley_name giving the emotion the smiley expresses. So
for a character named Bob the smileys should have codes

:Bob|cry: :Bob|glee: :Bob|amazed: etc.


Initially only the first smiley in each group is loaded and displayed when the user
is composing a post or PM. If the user mouses over a smiley, the rest of the smileys
in its group will be loaded and shown in a drop-down menu. Moving the mouse off of
the smiley hides the drop-down for its group. This way most smileys are only loaded
when the user wants to use them, allowing hundreds of smileys or more to be available
without making page load times unbearable.

Users can also mark smileys as favorites by dragging them into the Favorites area.
Favorites are always loaded immediately instead of waiting for the user to mouse over
them, so a forumgoer can always have quick access to the smileys they use the most.

The user can also drag smileys in each section to reorder them. The new orderings
will be saved in the user's browser so that the smileys always display in the chosen
order so long as the user revisits the page from the same computer and browser.

