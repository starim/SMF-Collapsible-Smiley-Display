/**
 * @author: Slixbits, © 2011
 * Licensed under LGPLv3 or later
 * 
 * Part of the Collapsible Smiley List plugin for SMF.
 * This file contains the Javascript class that provides a wrapper around the browser's HTML 5 Local Storage for
 * storing and retrieving user settings specific to the collapsible smiley list.
 * 
 */

 
function UserPreferences() {

	"use strict";
		
	var self = this;
	
	self.DEFAULTS = {"expandControls": true, "favorites": [], "unloaded": []};
	
	
	// function declaration section
	
	// saves a setting with the given name and value; value may be of any type but cannot be null
	// this function is meant for internal use by the UserPreferences class
	self.saveSetting = function(name, value) {
		if(value == null)
			throw new Error("Cannot save a null value for the setting \"" + name + "\".");
	
		if(self.LOCAL_STORAGE_AVAILABLE)
			localStorage[name] = $.toJSON(value);
	}
	
	// retrieves the value of the setting with the given name
	// this function is meant for internal use by the UserPreferences class
	self.getSetting = function(name) {
		if(self.LOCAL_STORAGE_AVAILABLE) {
		
			if(localStorage[name] == null)
				throw new Error("Cannot get the value of the uninitialized setting \"" + name + "\".");
			
			return $.parseJSON(localStorage[name]);
		}
		else
			return self.DEFAULTS[name];
	}
	
	
	// saves the passed boolean as the default visibility setting for the controls section (true to show the controls by default, false to hide by default)
	// null values cannot be saved
	self.setControlsVisibility = function(defaultExpandControls) {
		self.saveSetting("expandControls", defaultExpandControls);
	};
	
	// returns a boolean indicating whether the controls section should be visible by default
	self.getControlsVisibility = function() {
		return self.getSetting("expandControls");
	};
	
	
	// saves the passed array of smiley group names as the default Favorites list, overwriting the old setting
	self.setFavorites = function(favoritesList) {
		self.saveSetting("favorites", favoritesList);
	};
	
	// gets the current ordered list of favorites as an array of strings with the smiley group names
	self.getFavorites = function() {
		return self.getSetting("favorites");
	}
	
	// adds the specified smiley group to the list of favorites
	self.addFavorite = function(smileyGroupName) {
		if(self.LOCAL_STORAGE_AVAILABLE) {
			if(!self.isFavorite(smileyGroupName)) {
				var favoritesList = self.getFavorites();
				favoritesList.push(smileyGroupName);
				self.setFavorites(favoritesList);
			}
		}
	}
	
	// removes the specified smiley group from the list of favorites
	self.removeFavorite = function(smileyGroupName) {
		if(self.LOCAL_STORAGE_AVAILABLE) {
			if(self.isFavorite(smileyGroupName)) {
				var favoritesList = self.getFavorites();
				favoritesList.splice(favoritesList.indexOf(smileyGroupName), 1);
				self.setFavorites(favoritesList);
			}
		}
	}
	
	// returns a boolean indicating whether the user has marked the specified smiley group as a favorite
	self.isFavorite = function(smileyGroupName) {
		var favoritesList = self.getFavorites();
		return (favoritesList.indexOf(smileyGroupName) != -1);
	}
	
	// returns a boolean indicating whether the Favorites list is currently empty
	self.isFavoritesEmpty = function() {
		var favoritesList = self.getFavorites();
		return (favoritesList.length == 0);
	}
	
	// saves the passed array of smiley group names as the default order for the Unloaded list, overwriting the old setting
	self.setUnloadedOrder = function(unloadedList) {
		self.saveSetting("unloaded", unloadedList);
	};
	
	// gets the current ordered list of non-favorites as an array of strings with the smiley group names
	self.getUnloadedOrder = function() {
		return self.getSetting("unloaded");
	}
	
	
	// initialization section
	
	// check whether HTML5 Local Storage is available; if not, user preferences can't be saved and the default settings will always be used
	// this code snippet borrowed from http://diveintohtml5.org/storage.html#localstorage
	try {
		self.LOCAL_STORAGE_AVAILABLE = ('localStorage' in window && window['localStorage'] !== null);
  	} catch (e) {
		this.LOCAL_STORAGE_AVAILABLE = false;
  	}
  	
  	// initialize the Local Storage variables with defaut settings if there are no preexisting stored settings
  	if(self.LOCAL_STORAGE_AVAILABLE) {
  		for(var settingName in self.DEFAULTS) {
  			if(localStorage[settingName] == null)
  				self.saveSetting(settingName, self.DEFAULTS[settingName]);
  		}
	}
}
